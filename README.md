# Camp de terrain à Santec



## Introduction et généralités

Bienvenue sur le notre repo GitLab vous permettant d'accéder à l'ensemble des données nous ayant permis de mener les études relatives aux différentes journées de terrain.

## Utilisation

Pour consulter les résultats des tests statistiques de la rivière du Guillec, il suffit de cliquer sur le fichier [RcmdrMarkdown.html](https://gitlab.com/julescrch/santec/-/blob/main/RCommanderMarkdown.Rmd).

## Auteurs et remerciements
Ce dossier a été rédigé par Matthias Cornevin, Jules Créach et Eric Ruiz-Costa. Nos remerciements s'adressent aux enseignants de l'UE, à Julien Thébault pour la réalisation du tableau de la macrofaune benthique de la rivière du Guillec et à Gaultier Schaal pour le tableau de la macrofaune de l'estuaire du Guillec.


## License
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.

